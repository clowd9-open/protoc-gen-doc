module.exports = {
    branches: ["master","+([0-9])?(.{+([0-9]),x}).x", "dev", "main"],
    verifyConditions: [
      {
        path: "@semantic-release/gitlab",
        gitlabUrl: "https://gitlab.com"
      }
    ],
    prepare: [
    ],
    publish: [
      {
        path: "@semantic-release/gitlab",
        gitlabUrl: "https://gitlab.com"
      }
    ],
    success: false,
    fail: false,
    npmPublish: false,
    tarballDir: false,
    plugins: [
      '@semantic-release/commit-analyzer',
      '@semantic-release/release-notes-generator',
      [
        'semantic-release-slack-bot',
        {
          dryRun: true,
          notifyOnSuccess: true,
          notifyOnFail: true,
          branchesConfig: [
            {
              pattern: "master",
              notifyOnFail: true,
              notifyOnSuccess: true
            },
            {
              pattern: "*",
              notifyOnSuccess: false,
              notifyOnFail: true
            }
          ]
        }
      ]
    ]
  }
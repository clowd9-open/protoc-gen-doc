module gitlab.com/clowd9-open/protoc-gen-doc

go 1.14

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.4.2 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/Masterminds/sprig/v3 v3.2.2 // indirect
	github.com/aokoli/goutils v1.0.1 // indirect
	github.com/envoyproxy/protoc-gen-validate v0.3.0-java
	github.com/gogo/protobuf v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mwitkow/go-proto-validators v0.0.0-20180403085117-0950a7990007
	github.com/pseudomuto/protokit v0.2.0
	github.com/stretchr/testify v1.5.1
	google.golang.org/genproto v0.0.0-20210617175327-b9e0b3197ced
)

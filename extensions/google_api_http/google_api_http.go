package extensions

import (
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/options"
	"gitlab.com/clowd9-open/protoc-gen-doc/extensions"
	"google.golang.org/genproto/googleapis/api/annotations"
)

// HTTPRule represents a single HTTP rule from the (google.api.http) method option extension.
type HTTPRule struct {
	Method  string `json:"method"`
	Pattern string `json:"pattern"`
	Body    string `json:"body,omitempty"`
}

// HTTPExtension contains the rules set by the (google.api.http) method option extension.
type HTTPExtension struct {
	Rules []HTTPRule `json:"rules"`
}

func getRule(r *annotations.HttpRule) (rule HTTPRule) {
	switch r.GetPattern().(type) {
	case *annotations.HttpRule_Get:
		rule.Method = http.MethodGet
		rule.Pattern = r.GetGet()
	case *annotations.HttpRule_Put:
		rule.Method = http.MethodPut
		rule.Pattern = r.GetPut()
	case *annotations.HttpRule_Post:
		rule.Method = http.MethodPost
		rule.Pattern = r.GetPost()
	case *annotations.HttpRule_Delete:
		rule.Method = http.MethodDelete
		rule.Pattern = r.GetDelete()
	case *annotations.HttpRule_Patch:
		rule.Method = http.MethodPatch
		rule.Pattern = r.GetPatch()
	case *annotations.HttpRule_Custom:
		custom := r.GetCustom()
		rule.Method = custom.GetKind()
		rule.Pattern = custom.GetPath()
	}
	rule.Body = r.GetBody()
	return
}

type Operation struct {
	Description string `json:"description"`
	Summary string `json:"summary"`
	Tags []string `json:"tags"`
	Responses map[string]*options.Response `json:"responses"`
	OperationId string `json:"operationId"`
	Consumes []string `json:"consumes"`
	Produces []string `json:"produces"`
	Schemes []options.Scheme `json:"schemes"`
	Deprecated bool `json:"deprecated"`
}

type OperationResponse struct {
	Operations []Operation `json:"operations"`
}

type Schema struct {
	JsonSchema    *options.JSONSchema `json:"jsonSchema"`
	Discriminator string `json:"discriminator"`
	ReadOnly      bool `json:"readOnly"`
	ExternalDocs  *options.ExternalDocumentation `json:"externalDocs"`
	Example       string `json:"example"`
}

type SchemaResponse struct {
	Schemas []Schema `json:"schemas"`
}

// Note: JsonSchema supports more fiels - check them out options.JSONSchema - we just picked what we need for docs
type JsonSchema struct {
	Title            string 	`json:"title"`
	Description      string 	`json:"description"`
	Example          string 	`json:"example"`
	MaxLength        uint64 	`json:"maxLength"`
	MinLength        uint64 	`json:"minLength"`
	Pattern          string 	`json:"pattern"`
	Required         []string 	`json:"required"`
	Array            []string 	`json:"array"`
	Format           string		`json:"format"`
}

func init() {
	extensions.SetTransformer("google.api.http", func(payload interface{}) interface{} {
		var rules []HTTPRule
		rule, ok := payload.(*annotations.HttpRule)
		if !ok {
			return nil
		}

		rules = append(rules, getRule(rule))

		// NOTE: The option can only have one level of nested AdditionalBindings.
		for _, rule := range rule.AdditionalBindings {
			rules = append(rules, getRule(rule))
		}

		return HTTPExtension{Rules: rules}
	})

	extensions.SetTransformer("grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation", func(payload interface{}) interface{} {
		opt, ok := payload.(*options.Operation)
		if !ok {
			return nil
		}

		op := Operation{
			Description: opt.GetDescription(),
			Summary:     opt.GetSummary(),
			Tags:        opt.GetTags(),
			Responses:   opt.GetResponses(),
			OperationId: opt.GetOperationId(),
			Consumes:    opt.GetConsumes(),
			Produces:    opt.GetProduces(),
			Schemes:     opt.GetSchemes(),
			Deprecated:  opt.GetDeprecated(),
		}

		return OperationResponse{Operations: []Operation{op}}
	})

	extensions.SetTransformer("grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema", func(payload interface{}) interface{} {
		sch, ok := payload.(*options.Schema)
		if !ok {
			return nil
		}

		sc := Schema{
			JsonSchema:    sch.GetJsonSchema(),
			Discriminator: sch.GetDiscriminator(),
			ReadOnly:      sch.GetReadOnly(),
			ExternalDocs:  sch.GetExternalDocs(),
			Example:       sch.GetExample(),
		}
		return sc
	})

	extensions.SetTransformer("grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field", func(payload interface{}) interface{} {
		sch, ok := payload.(*options.JSONSchema)
		if !ok {
			return nil
		}

		jsonSchema := &JsonSchema{
			Title:            sch.GetTitle(),
			Description:      sch.GetDescription(),
			Example:          sch.GetExample(),
			MaxLength:        sch.GetMaxLength(),
			MinLength:        sch.GetMinLength(),
			Pattern:          sch.GetPattern(),
			Required:         sch.GetRequired(),
			Array:            sch.GetArray(),
			Format:           sch.GetFormat(),
		}
		return jsonSchema
	})
}